using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class CoinCollector : MonoBehaviour
{
    public float coinsCounter;
    public GameObject coinsCollected;
    void Start()
    {
        coinsCollected = GameObject.Find("CoinsCounter");
    }

    void Update()
    {
        coinsCollected.GetComponent<TMP_Text>().text = "Coins: " + coinsCounter + "/10";
        if (coinsCounter == 10)
        {
            SceneManager.LoadScene("SampleScene");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Coin")
        {
            coinsCounter++;
            Destroy(other.gameObject);
        }
    }
}
