using UnityEngine;

public class CoinAnimation : MonoBehaviour
{
    public float rotationSpeed = 100f; // Velocidad de rotaci�n en grados por segundo

    // Update es llamado una vez por frame
    void Update()
    {
        // Calcula la rotaci�n actual de la moneda
        Quaternion currentRotation = transform.rotation;

        // Calcula la rotaci�n adicional para este frame
        float rotationAmount = rotationSpeed * Time.deltaTime;
        Quaternion additionalRotation = Quaternion.Euler(0f, 0f, rotationAmount);

        // Combina las rotaciones para obtener la nueva rotaci�n de la moneda
        Quaternion newRotation = currentRotation * additionalRotation;

        // Aplica la nueva rotaci�n a la moneda
        transform.rotation = newRotation;
    }
}
